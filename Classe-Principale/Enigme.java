public class Enigme{

  private String nomEnigme;
  private String question;
  private String reponse;
  private byte[] img;
  private String aide;
  private boolean brouillon;

  /**
  *
  * @param nomEnigme nom de l'énigme
  * @param question question de l'énigme
  * @param reponse réponse de l'énigme
  * @param img image de l'énigme
  * @param aide texte d'aide de l'énigme
  * @param brouillon true si l'énigme est terminée
  */
  public Enigme(String nomEnigme, String question, String reponse, byte[] img, String aide, boolean brouillon){
    this.nomEnigme = nomEnigme;
    this.question = question;
    this.reponse = reponse;
    this.img = img;
    this.aide = aide;
    this.brouillon = brouillon;
  }

  /**
  *
  * @return la question de l'énigme
  */
  public String getQuestion(){
    return this.question;
  }

  /**
  *
  * @return la réponse de l'énigme
  */
  public String getReponse(){
    return this.reponse;
  }

  /**
  *
  * @return le texte d'aide de l'énigme
  */
  public String getAide(){
    return this.aide;
  }

  /**
  *
  * @param reponse reponse donnée par le joueur
  * @return true si la reponse donnée par le joueur correspond à la réponse de l'énigme
  */
  public boolean bonneReponse(String reponse){
    if(reponse.equals(this.reponse)){
      return true;
    }
    return false;
  }

}
