public interface Utilisateur {
    // Getters
    /**
     *
     * @return le pseudo de l'utilisateur
     */
    public String getPseudo();

    /**
     *
     * @return l'adresse email de l'utilisateur
     */
    public String getMail();

    /**
     *
     * @return le mot de passe de l'utilisateur
     */
    public String getMotDePasse();

    /**
     *
     * @return un boolean informant si l'utilisateur est connecté ou non
     */
    public boolean estConnecte();

    public int getId();

    public byte[] getAvatar();

    public String getRoleU();

    public void setConnexion(boolean connecte);
}
