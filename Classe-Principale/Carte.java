import java.util.List;

public class Carte{

  private String texteIntro;
  private String texteSucces;
  private String texteEchec;
  private List<Tuile> tuiles;
  private boolean brouillon;

  /**
  *
  * @param texteIntro texte d'introduction de la carte
  * @param texteSucces texte si le joueur réussit la carte
  * @param texteEchec texte si le joueur échoue la carte
  * @param brouillon true si la carte est terminée
  */
  public Carte(String texteIntro, String texteSucces, String texteEchec, List<Tuile> tuiles, boolean brouillon){
    this.texteIntro = texteIntro;
    this.texteSucces = texteSucces;
    this.texteEchec = texteEchec;
    this.tuiles = tuiles;
    this.brouillon = brouillon;
  }

  /**
  *
  * @return la liste des tuiles qui compossent la carte
  */
  public List<Tuile> getTuiles(){
    return this.tuiles;
  }

  /**
  *
  * @return le texte d'introduction de la carte
  */
  public String getIntroCarte(){
    return this.texteIntro;
  }

  /**
  *
  * @return le texte de succès de la carte
  */
  public String getSuccesCarte(){
    return this.texteSucces;
  }

  /**
  *
  * @return le texte d'echec de la carte
  */
  public String getEchecCarte(){
    return this.texteEchec;
  }

}
