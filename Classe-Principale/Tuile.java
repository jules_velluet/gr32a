public class Tuile{

  private int numTuile;
  private boolean joueurPresent;
  private Enigme enigme;
  private byte[] img;

  /**
  *
  * @param numTuile numéro de la tuile
  * @param joueurPresent true si un joueur est sur la tuile
  * @param enigme enigme présente sur la tuile, null si la tuile ne possède pas d'énigme
  * @param img image de la tuile
  */
  public Tuile(int numTuile, boolean joueurPresent, Enigme enigme, byte[] img){
    this.numTuile = numTuile;
    this.joueurPresent = joueurPresent;
    this.enigme = enigme;
    this.img = img;
  }

  /**
  *
  * @return le numéro de la tuile
  */
  public int getNum(){
    return this.numTuile;
  }

  /**
  *
  * @return true si un joueur est sur la tuile
  */
  public boolean getJoueurPresent(){
    return this.joueurPresent;
  }

  /**
  *
  * @return l'énigme présente sur la tuile ou null si la tuile de possède pas d'énigme
  */
  public Enigme getEnigme(){
    return this.enigme;
  }

}
