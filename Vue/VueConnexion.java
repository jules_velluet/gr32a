import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class VueConnexion extends Application {
    private Stage st;
    private TextField tPseudo;
    private TextField tMail;
    private PasswordField tMDP;
    private VueInscription vueInscription;
    private VueAccueilAdmin vueAccueilAdmin;
    private VueScenarios vueScenarios;
    private String typeCompte;
    private VueAccueil vueAccueil;
    private Utilisateur userConnecte;

    public VueConnexion(VueAccueil vueAccueil){
        this.vueInscription = new VueInscription(this);
        this.vueAccueilAdmin = new VueAccueilAdmin();
        this.vueScenarios = new VueScenarios(this, new VueInfosJoueur());
        this.vueAccueil = vueAccueil;
    }

    public VueInscription getVueInscription(){
        return this.vueInscription;
    }

    public Utilisateur getUserConnecte(){
        return this.userConnecte;
    }

    public VueAccueilAdmin getVueAccueilAdmin() {
        return vueAccueilAdmin;
    }

    public VueScenarios getVueScenarios() {
        return vueScenarios;
    }

    public Stage getSt() {
        return st;
    }

    public String getTypeCompte() {
        return typeCompte;
    }

    public VueAccueil getVueAccueil() {return this.vueAccueil;}

    public TextField gettPseudo(){
        return this.tPseudo;
    }

    public TextField gettMail(){
        return this.tMail;
    }

    public PasswordField gettMDP() {
        return tMDP;
    }

    public void setTypeCompte(String typeCompte){
        this.typeCompte = typeCompte;
    }

    public void setUserConnecte(Utilisateur user){this.userConnecte = user;}

    public void setScenePageConnexion(){
        // Initialisation Label
        Label lContent = new Label("Content de vous revoir !");
        lContent.setFont(Font.font("Arial", FontWeight.BOLD,30));
        lContent.setPadding(new Insets(50,0,0,0));

        // Initialisation TextField
        this.tPseudo = new TextField();
        this.tPseudo.setPromptText("Pseudo");
        this.tPseudo.setStyle("-fx-background-color: #c7c7c7ff; -fx-prompt-text-fill: #999999;-fx-background-radius: 15px");
        this.tPseudo.setPrefSize(350,40);

        this.tMail = new TextField();
        this.tMail.setPromptText("Adresse Mail");
        this.tMail.setStyle("-fx-background-color: #c7c7c7ff; -fx-prompt-text-fill: #999999;-fx-background-radius: 15px");
        this.tMail.setPrefSize(350,40);

        this.tMDP = new PasswordField();
        this.tMDP.setPromptText("Mot de Passe");
        this.tMDP.setStyle("-fx-background-color: #c7c7c7ff; -fx-prompt-text-fill: #999999;-fx-background-radius: 15px");
        this.tMDP.setPrefSize(350,40);

        // Initialisation VBox
        VBox vboxConnexion = new VBox();
        vboxConnexion.setSpacing(15);
        vboxConnexion.getChildren().addAll(this.tPseudo,this.tMail,this.tMDP,new ControleurConnexion(this));
        vboxConnexion.setAlignment(Pos.CENTER);

        //Initialisation BorderPane
        BorderPane borderPane = new BorderPane();
        borderPane.setTop(lContent);
        borderPane.setCenter(vboxConnexion);
        borderPane.setAlignment(lContent,Pos.TOP_CENTER);
        borderPane.setPadding(new Insets(40));
        Scene sceneConnexion = new Scene(borderPane);
        this.st.setScene(sceneConnexion);
    }

    public void setSt(Stage st){
        this.st = st;
    }

    public void start(Stage stage) {
        this.st = stage;
        stage.setTitle("L'Échappée Belle");
        stage.setWidth(500);
        stage.setHeight(500);
        this.setScenePageConnexion();
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
