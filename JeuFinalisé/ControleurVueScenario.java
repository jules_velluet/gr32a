import javafx.event.ActionEvent;
import javafx.event.EventHandler;



public class ControleurVueScenario implements EventHandler<ActionEvent> {

  private VueScenarios vueScenarios;

  public ControleurVueScenario(VueScenarios vueScenarios){
    this.vueScenarios = vueScenarios;
  }

  @Override
  public void handle(ActionEvent actionEvent){
    this.vueScenarios.sceneInitSuite();
  }

}
