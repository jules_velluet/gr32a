import jdk.jshell.execution.Util;

import javax.print.event.PrintJobEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.*;
import java.util.*;
import javafx.scene.control.Alert;

public class UtilisateurBD{
    private ConnexionMySQL laConnexion;

    private InsertionBlob ib;

    public UtilisateurBD(ConnexionMySQL laConnexion){
        this.laConnexion = laConnexion;
        this.ib = new InsertionBlob("sanna", "sanna", "servinfo-mariadb", "DBsanna");
    }

    public String insertUser(Utilisateur user) throws SQLException {
        PreparedStatement pst = this.laConnexion.prepareStatement("insert into UTILISATEUR values(?,?,?,?,?,?,?)");
        pst.setInt(1, user.getId());
        pst.setString(2, user.getPseudo());
        pst.setString(3, user.getMail());
        pst.setString(4, user.getMotDePasse());
        String activeut;
        if (user.estConnecte()){
            activeut = "O";
        } else {
            activeut = "N";
        }
        pst.setString(5, activeut);

        Blob b = this.laConnexion.createBlob();
        b.setBytes(1, user.getAvatar());
        pst.setBlob(6, b);
        int idRole;
        if (user.getRoleU().equals("Joueur")){
            idRole = 2;
        } else {
            idRole = 1;
        }
        pst.setInt(7, idRole);
        pst.executeUpdate();
        return user.getPseudo();
    }

    public Utilisateur createNewUser(String pseudo, String mail, String motDePasse, String cheminav, String role) throws SQLException {
        Utilisateur newUser;
        File f = new File(cheminav);
        byte[] newAv = null;
        try {
            newAv = Files.readAllBytes(f.toPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (role == "Joueur") {
            newUser = new Joueur(pseudo, mail, motDePasse, false,this.getMaxId()+1 ,newAv);
        } else {
            newUser = new Administrateur(pseudo, mail, motDePasse, false, this.getMaxId()+1,newAv);
        }
        return newUser;

    }


    public String removeUser(Utilisateur user) throws SQLException {
        PreparedStatement pst = this.laConnexion.prepareStatement("DELETE from UTILISATEUR where idut = ?");
        System.out.println(user.getId());
        pst.setInt(1, user.getId());
        pst.executeUpdate();
        return user.getPseudo();
    }
    public String updateUser(Utilisateur user) throws SQLException {
        PreparedStatement pst = this.laConnexion.prepareStatement("UPDATE UTILISATEUR natural join ROLE set pseudout = ?, emailut = ?, mdput = ?, avatarut = ?, nomRole = ?,activeut = ? where idut = ?");
        pst.setString(1, user.getPseudo());
        pst.setString(2, user.getMail());
        pst.setString(3, user.getMotDePasse());
        Blob b = this.laConnexion.createBlob();
        b.setBytes(1, user.getAvatar());
        pst.setBlob(4, b);
        pst.setString(5, user.getRoleU());
        String connecte = "N";
        if (user.estConnecte()){
            connecte = "O";
        }
        pst.setString(6,connecte);
        pst.setInt(7,user.getId());
        pst.executeUpdate();
        return user.getPseudo();
    }
    public boolean UserIsIn(Utilisateur user) throws SQLException {
        Statement st = this.laConnexion.createStatement();
        ResultSet rs = st.executeQuery("Select * from UTILISATEUR where idut = "+ user.getId());
        return (rs.next());
    }

    public int getMaxId() throws SQLException {
        Statement st = this.laConnexion.createStatement();
        ResultSet rs = st.executeQuery("Select ifnull(max(idut),0) as lemax from UTILISATEUR");
        rs.next();
        int res = rs.getInt("lemax");
        rs.close();
        return res;
    }
}
