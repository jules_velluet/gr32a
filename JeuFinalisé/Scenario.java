import java.util.List;

public class Scenario{

  private List<Carte> listeCarte;
  private int id;
  private String titre;
  private String resume;
  private int tempsMax;
  private int indexCarte;

  public Scenario(int id, String titre, String resume,int tempsMax, List<Carte> liste){
    this.id = id;
    this.titre = titre;
    this.resume = resume;
    this.tempsMax = tempsMax;
    this.listeCarte = liste;
    this.indexCarte = 0;
  }

  public List<Carte> getListe(){
    return this.listeCarte;
  }

  public int getTempsMax(){
    return this.tempsMax;
  }

  public int getIndex(){
    return this.indexCarte;
  }

  public void index(){
    this.indexCarte += 1;
  }

  public boolean finit(){
    if(this.indexCarte == this.listeCarte.size()){
      return true;
    }
    return false;
  }
}
