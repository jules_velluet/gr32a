import java.sql.*;
import java.util.*;
import java.util.Date;

public class PartieBD{
    private ConnexionMySQL laConnexion;

    public PartieBD(ConnexionMySQL laConnexion){
        this.laConnexion = laConnexion;

    }
    public List<Partie> listeParties() throws SQLException {
        List<Partie> liste = new ArrayList<>();
        Statement st = laConnexion.createStatement();
        ResultSet rs = st.executeQuery("Select * from PARTIE");
        Partie partie = null;
        while (rs.next()){
            boolean gagne = false;
            if (rs.getString("gagne") == "O"){gagne = true;}
            partie = new Partie(rs.getInt("idpa"),rs.getDate("datedebutpa"),rs.getTime("tpsresolution"),
            gagne,rs.getInt("idut"),rs.getInt("idsc"));
            liste.add(partie);
        }
        return liste;
    }

    public void insertPartie(int idpa,int idut, int idsc) throws SQLException{
      PreparedStatement pst = this.laConnexion.prepareStatement("insert into PARTIE value(?,?,?,?,?,?)");
      Date date = new Date();
      pst.setInt(1, idpa);
      pst.setDate(2, new java.sql.Date(date.getYear(), date.getMonth(), date.getDay()));
      pst.setTime(3, null);
      pst.setString(4, "N");
      pst.setInt(5, idut);
      pst.setInt(6, idsc);
      pst.executeUpdate();
    }

    public int newId() throws SQLException{
      Statement st;
      st = laConnexion.createStatement();
      ResultSet rs = st.executeQuery("select ifnull(max(idpa),0) max from PARTIE");
      rs.next();
      int res = rs.getInt("max");
      return res+1;
    }

    public void updatePartie(int idPa, Time temps) throws SQLException {
        Statement st;
        st = laConnexion.createStatement();
        st.executeUpdate("update PARTIE set gagne = 'O' where idpa =" + idPa);
    }

    public int nbPartie(int idut) throws SQLException{
      Statement st;
      st = laConnexion.createStatement();
      ResultSet rs = st.executeQuery("select ifnull(count(*),0) nb from PARTIE where idut=" + idut);
      rs.next();
      int res = rs.getInt("nb");
      rs.close();
      return res;
    }

    public int nbPartieGagnees(int idut) throws SQLException{
      Statement st;
      st = laConnexion.createStatement();
      ResultSet rs = st.executeQuery("select gagne from PARTIE where idut=" + idut);
      int res = 0;
      while(rs.next()){
        if(rs.getString("gagne").equals("O")){
          res++;
        }
      }
      rs.close();
      return res;
    }
}
