import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurlancementJeu implements EventHandler<ActionEvent> {
    private VueScenarios vueScenarios;


    public ControleurlancementJeu(VueScenarios vueScenarios){
        this.vueScenarios = vueScenarios;
    }

    public void handle(ActionEvent actionEvent){
        Jeu jeu = new Jeu();
        jeu.setVueScenarios(this.vueScenarios);
        jeu.start(this.vueScenarios.getSt());

    }

}
