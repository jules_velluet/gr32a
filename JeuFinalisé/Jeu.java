import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import java.util.List;
import java.util.ArrayList;
import java.sql.*;

public class Jeu extends Application {
    private Modele mod;
    private Stage st;
    private VueJeu vueJeu;
    private VueScenarios vueScenarios;
    public static int idPa;
    private int idSc;
    private int idUt;



    public void setVueScenarios(VueScenarios vueScenarios) {
        this.vueScenarios = vueScenarios;
    }

    public static void main(String[] args) {
        launch(args);
    }

    public void setSt(Stage st) {
        this.st = st;
    }

    public Stage getSt() {
        return st;
    }

    void setSceneComplet(VueJeu vueJeu){
        this.mod = vueJeu.getModele();
        BorderPane vueComplete = new BorderPane();
        vueComplete.setPadding(new Insets(5));
        vueComplete.setCenter(vueJeu);
        Scene s = new Scene(vueComplete);
        ControleurDeplacement controleur = new ControleurDeplacement(this.mod,this.vueJeu);
        s.setOnKeyPressed(controleur);
        this.st.setScene(s);
    }

    @Override
    public void start(Stage stage) {
        this.idSc = 1;
        this.idUt = this.vueScenarios.getVueInfosJoueur().getJoueur().getId();
        PartieBD partieBD = new PartieBD(VueAccueil.connexionMySQL);
        try{
          this.idPa = partieBD.newId();
          partieBD.insertPartie(idPa,idUt,idSc);
        }catch(SQLException e){

        }

        this.st=stage;
        vueJeu=new VueJeu(this,this.mod,idSc);
        vueJeu.setVueScenarios(this.vueScenarios);
        this.st.setWidth(660);
        this.st.setHeight(680);
        setSceneComplet(vueJeu);
        this.st.show();
    }

    public int getIdPa(){
      return this.idPa;
    }
}
