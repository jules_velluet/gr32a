import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;

/**
 * Contrôleur du compte à rebours
 */
public class ActionTemps implements EventHandler<ActionEvent> {
    /**
     * temps enregistré lors du dernier événement
     */
    private long tempsPrec;

    /**
     * Vue du compte à rebours
     */
    private CompteARebours compteARebours;
    /**
     * Modele du jeu de taquin
     */
    private VueJeu vueJeu;

    private Modele mod;
    /**
     * temps restant du compte à rebours: c'est un peu le modèle du compre à rebours
     */
    private long tempsRestant;

    /**
     * Constructeur du contrôleur du compte à rebours
     * notez que le modèle du compte à rebours est tellement simple
     * qu'il est inclus dans le contrôleur sous la forme de la variable tempsRestant
     * @param compteARebours Vue du compte à rebours
     * @param vueTaquin Modèle du jeu de taquin
     */
    ActionTemps (CompteARebours compteARebours, VueJeu vueJeu, Modele mod,long tempsRestant){
        this.compteARebours=compteARebours;
        this.vueJeu=vueJeu;
        this.mod=mod;
        this.tempsPrec=-1;
        this.tempsRestant=tempsRestant;
    }

    /**
     * Actions à effectuer tous les pas de temps
     * essentiellement mesurer le temps écoulé depuis la dernière mesure
     * et mise à jour du temps restant
     * Il faut aussi indiquer à l'utilisateur quand le temps est écoulé
     * Il faut arreter le compte à rebours si l'utilisateur a gagné
     * @param actionEvent événement Action
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        long tpsActuel=System.currentTimeMillis(); // recupération de l'heure système
        // A IMPLEMENTER
        if (this.tempsPrec!=-1){
            this.tempsRestant -= (tpsActuel-tempsPrec);
        }
        this.compteARebours.setTime(tempsRestant);
        tempsPrec = tpsActuel;
        if (this.tempsRestant <= 0){
            compteARebours.stop();
            this.compteARebours.resetTime();
            this.tempsPrec = -1;
            Alert al = new Alert(Alert.AlertType.INFORMATION);
            al.setHeaderText("Pas de chance");
            al.setContentText("Vous avez perdu.");
            al.show();
        }
    }

    /**
     * Reinitialise le temps restant (normalement dans le modèle)
     */
    public void reset(long tempsRestant){
        // A IMPLEMENTER
        this.tempsRestant = tempsRestant;
    }

    public long getTempsRestant(){
      return this.tempsRestant;
    }
}
