import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.paint.Color;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class VueGestionPartiesAdmin extends Application{

    private Stage st;
    private TextField tcherchePartie;
    private VueConnexion vueConnexion;
    private PartieBD partieBD;

    public static void main(String[] args) {
        launch(args);
    }

    public VueConnexion getVueConnexion() {
        return vueConnexion;
    }

    public void setVueConnexion(VueConnexion vueConnexion) {
        this.vueConnexion = vueConnexion;
    }

    public void setSt(Stage st){
        this.st = st;
    }
    public Stage getSt(){
        return this.st;
    }

    public void setScenePageGestionPartiesAdmin(){
        this.partieBD = new PartieBD(VueAccueil.connexionMySQL);
        List<Partie> ListePartie = null;
        try {
            ListePartie = this.partieBD.listeParties();
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }

        // Initialisation du bouton retour
        VBox vRetour = new VBox();
        Image img = new Image("http://freevector.co/wp-content/uploads/2014/04/59098-return-arrow-curve-pointing-left.png");
        ImageView view = new ImageView(img);
        view.setFitHeight(20);
        view.setPreserveRatio(true);
        Button bRetour = new Button();
        bRetour.setOnAction(new ControleurRetourAdminJoueursParties(this));
        bRetour.setTranslateX(5);
        bRetour.setTranslateY(5);
        bRetour.setPrefSize(20,20);
        bRetour.setGraphic(view);
        vRetour.getChildren().add(bRetour);
        vRetour.setAlignment(Pos.TOP_LEFT);
        vRetour.setPadding(new Insets(-30,0,0,0));

        // Initialisation TextFields, textes ...
        Label lGestionJoueur = new Label("Gestion des parties");
        lGestionJoueur.setPrefSize(50000,20);
        lGestionJoueur.setAlignment(Pos.CENTER);
        lGestionJoueur.setStyle("-fx-text-fill: #000000; -fx-background-color: #ccccccff");

        Label lrecherche = new Label("Rechercher une partie");
        lrecherche.setFont(Font.font("Arial",20));
        this.tcherchePartie = new TextField();
        this.tcherchePartie.setMaxSize(250,35);

        Label lJoueurs = new Label("Listes des Parties :");
        lJoueurs.setFont(Font.font("Arial",FontWeight.BOLD,25));


        // Initialisation vBox et Scene
        BorderPane borderPane = new BorderPane();
        HBox chercherPartie = new HBox();
        VBox vBoxGestionParties = new VBox();

        chercherPartie.getChildren().addAll(lrecherche,this.tcherchePartie);
        chercherPartie.setAlignment(Pos.CENTER);
        chercherPartie.setSpacing(25);
        vBoxGestionParties.getChildren().addAll(lGestionJoueur,vRetour,chercherPartie,lJoueurs);
        vBoxGestionParties.setAlignment(Pos.TOP_CENTER);
        borderPane.setTop(vBoxGestionParties);
        List<Partie> newliste = ListePartie;

        VBox vb = new VBox();
        GridPane gd = new GridPane();
        for (int i = 0; i < 3; i++){
            for (int y = 0; y < 2; y++){
                // Initialisation des labels
                Label titre = new Label("Titre");
                titre.setFont(Font.font("Arial", 15));
                titre.setAlignment(Pos.TOP_LEFT);
                Label DateDebut = new Label("(Date début)");
                DateDebut.setFont(Font.font("Arial", 10));
                Label TempsResolution = new Label("(Temps de résolution)");
                TempsResolution.setFont(Font.font("Arial", 10));
                Label Resultat = new Label("(Resultat)");
                Resultat.setFont(Font.font("Arial", 10));
                Label IdUtilisateur = new Label("(ID de l'utilisateur)");
                IdUtilisateur.setFont(Font.font("Arial",FontWeight.BOLD, 10));
                IdUtilisateur.setAlignment(Pos.BOTTOM_RIGHT);
                Label IdScenario = new Label("(ID du Scénario)");
                IdScenario.setFont(Font.font("Arial",FontWeight.BOLD, 10));
                IdScenario.setAlignment(Pos.BOTTOM_RIGHT);

                if (!newliste.isEmpty()){
                    Partie partie = newliste.get(0);
                    String gagne = "Défaite";
                    if (partie.getVictoire()){
                        gagne = "Victoire";
                    }
                    titre.setText("Partie n°"+partie.getIdPartie());
                    DateDebut.setText("Début : "+partie.getDateDebut());
                    TempsResolution.setText("Terminée après : "+partie.getTempsResolution());
                    Resultat.setText("Résultat : "+gagne);
                    IdUtilisateur.setText("Utilisateur n°"+partie.getIdUtilisateur());
                    IdScenario.setText("Scénario n°"+partie.getIdUtilisateur());
                    newliste.remove(0);
                }


                // Initialistion scénario
                VBox textesCentraux = new VBox();
                textesCentraux.getChildren().addAll(titre,DateDebut,TempsResolution,Resultat);
                textesCentraux.setSpacing(2);
                textesCentraux.setAlignment(Pos.CENTER);
                VBox textePseudo = new VBox();
                textePseudo.getChildren().add(titre);
                textePseudo.setAlignment(Pos.TOP_LEFT);
                VBox texteIds = new VBox();
                texteIds.getChildren().addAll(IdUtilisateur,IdScenario);
                texteIds.setAlignment(Pos.BOTTOM_LEFT);
                BorderPane borderPane1 = new BorderPane();
                borderPane1.setTop(textePseudo);
                borderPane1.setCenter(textesCentraux);
                borderPane1.setBottom(texteIds);
                borderPane1.setPadding(new Insets(40));
                borderPane1.setStyle("-fx-border-radius : 10");

                Button bScenario = new Button();
                bScenario.setGraphic(borderPane1);
                bScenario.setStyle("-fx-background-color: white; -fx-border-color: black; -fx-border-width: 1px");
                bScenario.setPadding(new Insets(-15));
                gd.add(bScenario,i,y);
                gd.setHgap(10);
                gd.setVgap(10);
            }
        }
        gd.setAlignment(Pos.BOTTOM_CENTER);
        vBoxGestionParties.getChildren().add(gd);
        vBoxGestionParties.setSpacing(35);

        Scene sceneGestionPartiesAdmin = new Scene(borderPane);
        this.st.setScene(sceneGestionPartiesAdmin);

    }

    public void start(Stage stage) {
        this.st = stage;
        stage.setTitle("L'Échappée Belle - Administrateur");
        stage.setWidth(600);
        stage.setHeight(600);
        this.setScenePageGestionPartiesAdmin();
        stage.show();
    }
}
