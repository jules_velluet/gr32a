import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurInterneScenarios implements EventHandler<ActionEvent>{

private VueScenarios vueScenarios;

  public ControleurInterneScenarios(VueScenarios vueScenarios){
    this.vueScenarios = vueScenarios;
  }

  public void handle(ActionEvent actionEvent){
    this.vueScenarios.detailsScene();
  }

}
