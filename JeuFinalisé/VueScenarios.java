import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import java.sql.SQLException;

public class VueScenarios extends Application {

    private Stage st;
    private VueInfosJoueur vueInfosJoueurs;
    private VueConnexion vueConnexion;
    private int idScenario;
    private ScenarioBD scenarioBD;
    private VBox scene;
    private BorderPane bScene1;

    public static void main(String[] args) {
        launch(args);
    }

    public VueInfosJoueur getVueInfosJoueur(){
      return this.vueInfosJoueurs;
    }

    public VueScenarios(VueConnexion vueConnexion, VueInfosJoueur vueInfosJoueurs){
        this.vueInfosJoueurs = vueInfosJoueurs;
        this.vueConnexion = vueConnexion;
        this.idScenario = 1;
        try{
            VueAccueil vueAccueil = new VueAccueil();
            VueAccueil.initConnexionMySQL();
            ConnexionMySQL c = VueAccueil.connexionMySQL;
            this.scenarioBD = new ScenarioBD(c);
        }catch(Exception e){

        }

    }

    public VueInfosJoueur getVue() {
        return this.vueInfosJoueurs;
    }

    public Stage getSt() {
        return st;
    }

    public void setSt(Stage st){
        this.st = st;
    }

    public void sceneInitSuite(){
        this.st.setScene(SceneInit(this.st));
        this.st.show();
    }
    public VueConnexion getVueConnexion(){
        return this.vueConnexion;
    }

    public Scene SceneInit(Stage st){
        this.st = st;

        // Initialisation du bouton déconnexion
        Button bDeco = new Button("Déconnexion");
        bDeco.setTranslateX(5);
        bDeco.setTranslateY(5);
        bDeco.setPrefSize(110,20);
        bDeco.setAlignment(Pos.CENTER);
        ControleurDeconnexion controleurDeconnexion = new ControleurDeconnexion(this.vueConnexion.getVueAccueil());
        bDeco.setOnAction(controleurDeconnexion);

        // Initialisation texte
        Label lScenarios = new Label("Scénarios");
        lScenarios.setFont(Font.font("Arial", FontWeight.BOLD,40));
        lScenarios.setPadding(new Insets(-30,0,0,0));

        // Initialisation Liste de Scénarios
        HBox liste = new HBox();
        liste.setAlignment(Pos.CENTER);
        liste.setSpacing(20);

        this.bScene1 = this.devantScene(this.idScenario);
        Button flecheGauche = this.flecheGauche();
        Button flecheDroite = this.flecheDroite();

        // Initialisation du bouton retour
        VBox vRetour = new VBox();
        Image img = new Image("http://freevector.co/wp-content/uploads/2014/04/59098-return-arrow-curve-pointing-left.png");
        ImageView view = new ImageView(img);
        view.setFitHeight(20);
        view.setPreserveRatio(true);
        Button bRetour = new Button();
        bRetour.setTranslateX(5);
        bRetour.setTranslateY(5);
        bRetour.setPrefSize(20,20);
        bRetour.setGraphic(view);
        vRetour.getChildren().add(bRetour);
        vRetour.setAlignment(Pos.TOP_LEFT);

        // Bouton Informations
        Button infos = new Button("+ INFOS");
        infos.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        infos.setTranslateX(380);
        infos.setTranslateY(5);
        infos.setPrefHeight(30);
        infos.setPrefWidth(100);
        infos.setStyle("-fx-background-border : 10");
        infos.setOnAction(new ControleurScenarios(this));

        // Boutons
        HBox hbBoutons = new HBox();
        hbBoutons.getChildren().addAll(bDeco, infos);


        liste.getChildren().addAll(flecheGauche, bScene1, flecheDroite);


        // Initialisation Bouton Jouer
        Button bJouer = new Button("Jouer");
        bJouer.setStyle("-fx-background-color : #6aa84fff ; -fx-background-radius: 20");
        bJouer.setTextFill(new Color(1,1,1,1));
        bJouer.setFont(Font.font("Arial", FontWeight.BOLD,20));
        bJouer.setPrefHeight(50);
        bJouer.setPrefWidth(150);
        bJouer.setOnAction(new ControleurlancementJeu(this));


        // Initialisation Scène
        this.scene = new VBox();
        this.scene.getChildren().addAll(hbBoutons,lScenarios, liste, bJouer);
        this.scene.setSpacing(20);
        this.scene.setAlignment(Pos.CENTER);

        return new Scene(scene);
    }

    // Flèches
    public Button flecheGauche() {
        Button flecheGauche = new Button();
        flecheGauche.setPrefHeight(100);
        flecheGauche.setPrefWidth(50);
        flecheGauche.setStyle("-fx-background-color: grey; -fx-shape: 'M 400 250 L 400 550 L 250 400 Z'");
        return flecheGauche;
    }

    public Button flecheDroite() {
        Button flecheDroite = new Button();
        flecheDroite.setPrefHeight(100);
        flecheDroite.setPrefWidth(50);
        flecheDroite.setStyle("-fx-background-color : grey ; -fx-shape : 'M 400 250 L 400 550 L 550 400 Z'");
        return flecheDroite;
    }

    // Méthode Devant Page Scénario
    public BorderPane devantScene(int idScenario) {
        // Initialisation Contenu Scénario
        BorderPane scenario = new BorderPane();
        try{
            Label lTitre = new Label(this.scenarioBD.nomScenario(this.idScenario));
            lTitre.setFont(Font.font("Arial", 30));

            Image image = this.scenarioBD.icone(this.idScenario);
            VBox vbT = new VBox();
            vbT.getChildren().add(lTitre);
            vbT.getChildren().add(new ImageView(image));
            vbT.setAlignment(Pos.CENTER);
            vbT.setPadding(new Insets(50,0,0,0));

            HBox hbT = new HBox();
            hbT.getChildren().add(vbT);
            hbT.setAlignment(Pos.CENTER);
            hbT.setPadding(new Insets(10,0,0,0));

            // Bouton Détails
            Button bDetails = new Button("+ Détails");
            bDetails.setOnAction(new ControleurInterneScenarios(this));
            bDetails.setStyle("-fx-background-radius : 30");
            bDetails.setFont(Font.font("Arial", FontWeight.BOLD,15));
            HBox hbD = new HBox();
            hbD.getChildren().add(bDetails);
            hbD.setAlignment(Pos.BOTTOM_RIGHT);
            hbD.setPadding(new Insets(0,10,10,0));


            // Initialisation Scénario
            scenario.setTop(hbT);
            scenario.setBottom(hbD);
            scenario.setPrefHeight(400);
            scenario.setPrefWidth(400);
            scenario.setStyle("-fx-border-color : black ; -fx-border-width : 2px");
        }catch(SQLException e){

        }
        return scenario;
    }

    // Méthode Détails Page Scénario
    public BorderPane detailsScene() {
        // Initialisation Contenu Scénario
        try{
          Label lTitre = new Label(this.scenarioBD.nomScenario(this.idScenario));
          lTitre.setFont(Font.font("Arial", 30));

          Label tempsDispo = new Label("Temps Disponible :" + this.scenarioBD.tpsMaxScBis(this.idScenario));
          tempsDispo.setFont(Font.font("Arial", 10));
          Label resumeScenario = new Label(this.scenarioBD.resumeSc(this.idScenario));
          resumeScenario.setFont(Font.font("Arial", 10));
          Label dateMiseEnLigne = new Label("Date mise en ligne" + this.scenarioBD.dateMiseEnLigne(this.idScenario));
          dateMiseEnLigne.setFont(Font.font("Arial", 10));

          VBox vb = new VBox();
          vb.getChildren().addAll(resumeScenario, tempsDispo, dateMiseEnLigne);


          // Bouton Retour
          Button retour = new Button("Retour");
          retour.setOnAction(new ControleurVueScenario(this));
          retour.setFont(Font.font("Arial", FontWeight.BOLD,15));
          retour.setUnderline(true);

          // Initialisation Scénario
          BorderPane scenario = new BorderPane();
          scenario.setTop(lTitre);
          scenario.setCenter(vb);
          scenario.setBottom(retour);
          scenario.setPrefHeight(300);
          scenario.setPrefWidth(110);

         HBox test = new HBox();
         test.getChildren().add(scenario);
         test.setAlignment(Pos.CENTER);

         this.scene.getChildren().setAll(test);

          return scenario;
        }catch(SQLException e ){
          System.out.println(e.getMessage());
        }
        return null;
    }

    public void start(Stage stage){
        this.st = stage;
        this.st.setTitle("L'Échappée Belle");
        this.st.setWidth(600);
        this.st.setHeight(600);
        this.st.setScene(SceneInit(this.st));
        this.st.show();
    }
}
