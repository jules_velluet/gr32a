import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import java.sql.*;

public class ControleurRetourVueScenario implements EventHandler<ActionEvent> {

    private VueScenarios vueScenarios;
    private Stage stage;
    private PartieBD partieBD;
    private VueJeu vueJeu;
    private Time time;

    public ControleurRetourVueScenario(VueScenarios vueScenarios, Stage stage, VueJeu vueJeu,Long tempsMax, Long tempsRestant){
        this.vueScenarios = vueScenarios;
        this.stage = stage;
        this.partieBD = new PartieBD(VueAccueil.connexionMySQL);
        this.vueJeu = vueJeu;
        Long tempsMillisec = tempsMax-tempsRestant;
        int tempsEnSecondes = tempsMillisec.intValue()/1000;
        int nbHeures=tempsEnSecondes/3600;
        int nbMinutes = (tempsEnSecondes-nbHeures*3600)/60;
        int nbSecondes=tempsEnSecondes%60;
        this.time = new Time(nbHeures, nbMinutes, nbSecondes);
    }

    @Override
    public void handle(ActionEvent actionEvent){
      try{
        this.partieBD.updatePartie(Jeu.idPa,this.time);
      }catch(SQLException e){
        System.out.println(e.getMessage());
      }
      this.vueScenarios.sceneInitSuite();
      this.stage.close();

    }
}
