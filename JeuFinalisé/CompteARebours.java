import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.util.Duration;

/**
 * Permet de gérer un label associé à une Timeline pour afficher un temps écoulé
 */
public class CompteARebours extends Label{

    /**
     * temps initial du compte à rebours en milisecondes
     */
    private long tempsInitial;

    /**
     * timeline qui va gérer le temps
     */
    private Timeline timeline;
    /**
     * la fenêtre de temps
     */
    private KeyFrame keyFrame;
    /**
     * le contrôleur associé au chronomètre
     */
    private ActionTemps actionTemps;

    /**
     * Accès à la vue du jeu (permettant notamment au controleur de désactiver le jeu
     */
    private VueJeu vueJeu;

    private String message;

    /**
     * Constructeur permettant de créer le chronomètre
     * avec un label initialisé à la valeur du temps initial
     * Ce constructeur créer la Timeline, la KeyFrame et le contrôleur
     */
    CompteARebours(long tempsRestant, VueJeu vueJeu, Modele mod,String message){
        super(message + "\n 0:0:0");
        this.tempsInitial = tempsRestant;
        actionTemps=new ActionTemps(this,vueJeu,mod,tempsRestant);
        keyFrame = new KeyFrame(Duration.millis(100),actionTemps);
        timeline=new Timeline(keyFrame);
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
        this.vueJeu=vueJeu;
        this.message = message;
    }

    /**
     * Permet au controleur de mettre à jour le label
     * la durée est affichée sous la forme h:m:s
     * @param tempsMillisec la durée depuis à afficher
     */
    public void setTime(long tempsMillisec){
        // A IMPLEMENTER
        long tempsEnSecondes = tempsMillisec/1000;
        long nbHeures=tempsEnSecondes/3600;
        long nbMinutes = (tempsEnSecondes-nbHeures*3600)/60;
        long nbSecondes=tempsEnSecondes%60;
        this.setText(message+"\n "+nbHeures+":"+nbMinutes+":"+nbSecondes);
    }

    /**
     * Permet de démarrer le compte à rebours
     */
    public void start(){
        // A IMPLEMENTER
        timeline.play();
    }

    /**
     * Permet d'arrêter le compte à rebours
     */
    public void stop(){
        timeline.stop();
    }

    /**
     * Permet de remettre le compte à rebours au temps initial
     */
    public void resetTime(){
        // A IMPLEMENTER
        actionTemps.reset(tempsInitial);
     }

    public ActionTemps getActionTemps(){
      return this.actionTemps;
    }
}
