import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;

import java.io.File;
import java.sql.SQLException;

public class ControleurInscription implements EventHandler<ActionEvent> {
    public VueConnexion vueConnexion;
    private UtilisateurBD userBD;
    private VueInscription vueInscription;
    private String pseudo;
    private String mail;
    private String mdp;
    private String confirmMdp;
    private String Chemin;


    public ControleurInscription(VueConnexion vueConnexion){
        this.userBD = new UtilisateurBD(VueAccueil.connexionMySQL);
        this.vueConnexion = vueConnexion;
        this.vueInscription = vueConnexion.getVueInscription();

    }
    @Override
    public void handle(ActionEvent actionEvent){
        this.Chemin = this.vueInscription.getChemin();
        this.pseudo = this.vueInscription.getTpseudo().getText();
        this.mail = this.vueInscription.getTmail().getText();
        this.mdp = this.vueInscription.getTmdp().getText();
        this.confirmMdp = this.vueInscription.getTconfirmMDP().getText();
        if (this.mdp.equals(this.confirmMdp)) {
            int newID;
            Utilisateur user = null;
            try {
                newID = userBD.getMaxId();
                System.out.println(vueConnexion.getTypeCompte());
                if(this.Chemin != null){
                  if (vueConnexion.getTypeCompte().equals("Admin")) {
                      user = userBD.createNewUser(this.pseudo, this.mail, this.mdp,this.Chemin,"Admin");
                  } else if (vueConnexion.getTypeCompte().equals("Joueur")) {
                      user = userBD.createNewUser(this.pseudo, this.mail, this.mdp,this.Chemin,"Joueur");
                  }
                  userBD.insertUser(user);
                  this.vueConnexion.setScenePageConnexion();
                }else{
                  Alert al = new Alert(Alert.AlertType.ERROR);
                  al.setTitle("Erreur");
                  al.setHeaderText("Problème à l'inscription");
                  al.setContentText("Veuillez choisir une image.");
                  al.showAndWait();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                Alert al = new Alert(Alert.AlertType.ERROR);
                al.setTitle("Erreur");
                al.setHeaderText("Problème à l'inscription");
                al.setContentText("Un problème semble persister lors de la tentative d'inscription', veuillez prévenir un administrateur.");
                al.showAndWait();
            }
        } else{
            Alert al = new Alert(Alert.AlertType.ERROR);
            al.setTitle("Erreur");
            al.setHeaderText("Mot de passe erroné.");
            al.setContentText("Les deux mots de passes inscrit sont différents.");
            al.showAndWait();
        }

    }

}
