import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import java.util.Vector;
import java.util.List;
import java.util.ArrayList;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.FontWeight;
import javafx.scene.paint.Color;

public class VueEnigme{

  private Modele mod;
  private TextField texteReponse;
  private Stage nw;

  public VueEnigme(Modele mod, Enigme enigme){
    this.mod = mod;
    BorderPane pane = new BorderPane();
    Label titre = new Label(enigme.getNom());
    titre.setFont(Font.font("Arial", FontWeight.BOLD,40));
    FlowPane fptitre = new FlowPane();
    fptitre.getChildren().add(titre);
    fptitre.setAlignment(Pos.CENTER);
    pane.setTop(fptitre);
    Label texteEnigme = new Label(this.aLaLigne(enigme.getQuestion()));
    texteEnigme.setFont(Font.font("Arial",15));
    VBox box = new VBox();
    FlowPane fptexte = new FlowPane();
    fptexte.getChildren().add(texteEnigme);
    fptexte.setAlignment(Pos.CENTER);
    fptexte.setMaxSize(400,400);
    box.getChildren().add(fptexte);
    box.getChildren().add(new ImageView(enigme.getImage()));
    box.setAlignment(Pos.CENTER);
    Label rep = new Label("Réponse: ");
    Button ind = new Button("indice");
    ControleurIndice con = new ControleurIndice(enigme.getAide());
    ind.setOnAction(con);
    ind.setStyle("-fx-background-color : #6aa84fff ; -fx-background-radius: 20");
    ind.setTextFill(new Color(1,1,1,1));
    ind.setFont(Font.font("Arial", FontWeight.BOLD,15));
    ind.setPrefHeight(25);
    ind.setPrefWidth(75);
    this.texteReponse = new TextField();
    VBox v = new VBox();
    FlowPane fp = new FlowPane();
    fp.getChildren().addAll(rep,ind);
    fp.setHgap(260);
    v.getChildren().addAll(fp,this.texteReponse);
    pane.setBottom(v);
    pane.setCenter(box);
    Scene sc = new Scene(pane,400,400);
    ControleurReponse controleur = new ControleurReponse(this.mod,this,enigme);
    sc.setOnKeyPressed(controleur);
    this.nw = new Stage();
    this.nw.setScene(sc);
    this.nw.setTitle("Enigme");
    this.nw.setScene(sc);
    this.nw.show();
    this.texteReponse.requestFocus();
  }

  public String aLaLigne(String texte){
    String [] mots = texte.split(" ");
    String res = "";
    int motNb = 0;
    for(String mot: mots){
      res += mot + " ";
      if(motNb != 0 && motNb%5 == 0){
        res+= "\n";
      }
      motNb++;
    }
    return res;
  }

  public String getReponse(){
    return this.texteReponse.getText();
  }

  public TextField getTextField(){
    return this.texteReponse;
  }

  public void close(){
    this.nw.close();
  }

}
