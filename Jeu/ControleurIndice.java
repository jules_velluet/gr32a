import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert.AlertType;

public class ControleurIndice implements EventHandler<ActionEvent> {

    private String indice;

    public ControleurIndice(String indice){
      this.indice = indice;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
      Alert alert = new Alert(AlertType.INFORMATION);
      alert.setTitle("Enigme");
      alert.setHeaderText("Indice:");
      alert.setContentText(indice);
      alert.showAndWait();
    }
}
