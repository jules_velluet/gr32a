import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;

public class ControleurDeplacement implements EventHandler<KeyEvent> {
    private Modele mod; // modèle du jeu
    private VueJeu vueJeu; // vue du jeu

    /**
     * constructeur
     * @param mod modèle du jeu
     * @param vueJeu vue du jeu
     * @param lig ligne où se situe le joueur dans la vue
     * @param col colonne où se situe le joueur dans la vue
     */
    public ControleurDeplacement(Modele mod, VueJeu vueJeu) {
        this.mod=mod;
        this.vueJeu=vueJeu;
    }

    @Override
    public void handle(KeyEvent keyEvent) {
      String touche = keyEvent.getCode().toString();
      if(touche.equals("UP") || touche.equals("DOWN") || touche.equals("RIGHT") || touche.equals("LEFT"))
        vueJeu.update(touche);
    }
}
