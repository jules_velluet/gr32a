import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class ControleurReponse implements EventHandler<KeyEvent> {
    private Modele mod; // modèle du jeu
    private VueEnigme vueEnigme; // vue du jeu
    private Enigme enigme;

    /**
     * constructeur
     * @param mod modèle du jeu
     * @param vueJeu vue du jeu
     */
    public ControleurReponse(Modele mod, VueEnigme vueEnigme, Enigme enigme) {
        this.mod=mod;
        this.vueEnigme=vueEnigme;
        this.enigme=enigme;
    }

    @Override
    public void handle(KeyEvent keyEvent) {
      if(keyEvent.getCode().toString().equals("ENTER")){
        if(!enigme.bonneReponse(this.vueEnigme.getReponse())){
          Alert alert = new Alert(AlertType.INFORMATION);
          alert.setTitle("Enigme");
          alert.setHeaderText("Resultat:");
          alert.setContentText("C'est une mauvaise réponse.");
          alert.showAndWait();
          this.vueEnigme.getTextField().requestFocus();
        }else{
          Alert alert = new Alert(AlertType.INFORMATION);
          alert.setTitle("Enigme");
          alert.setHeaderText("Resultat:");
          alert.setContentText("Bravo !! c'est une bonne réponse.");
          alert.showAndWait();
          this.vueEnigme.close();
          this.enigme.reussi();
        }
      }

    }
}
