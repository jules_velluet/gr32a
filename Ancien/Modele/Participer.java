import java.sql.*;

public class Participer{

  private ConnexionMySQL laConnexion;

  public Participer(ConnexionMySQL laConexion){
    this.laConnexion = laConexion;
  }

  public int numOrdre(int idca, int idsc) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select numordre from PARTICIPER where idCa="+idca+"and idSc="+idsc);
    rs.next();
    int res = rs.getInt("numordre");
    rs.close();
    return res;
  }
}
