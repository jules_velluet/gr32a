import java.sql.*;
import java.util.List;

public class ConnexionBD{

  private ConnexionMySQL laConexion;
  private Utilisateur user;
  private UtilisateurBD userBd;

  public ConnexionBD(ConnexionMySQL laConexion, Utilisateur user){
    this.laConexion = laConexion;
    this.user = user;
    this.userBd = new UtilisateurBD(laConexion);
  }

  public boolean connecter() throws new SQLExeption{
    if (this.userBd.UserIsIn(this.user)){
      return true;
    }
    else {
      return false;
    }
  }
}
