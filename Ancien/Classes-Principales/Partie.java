import java.sql.*;
import java.util.*;

public class Partie {

    private int idpa;
    private java.sql.Date dateDebut;
    private Time tmpsResolution;
    private boolean gagne;
    private int idut;
    private int idsc;

    public Partie(int idpa, java.sql.Date dateDebut, Time tmpsResolution, boolean gagne, int idut, int idsc){
        this.idpa = idpa;
        this.dateDebut = dateDebut;
        this.tmpsResolution = tmpsResolution;
        this.gagne = gagne;
        this.idut = idut;
        this.idsc = idsc;
    }

    // Getters
    public int getIdPartie() {return this.idpa;}
    public java.sql.Date getDateDebut() {return this.dateDebut;}
    public Time getTempsResolution() {return this.tmpsResolution;}
    public boolean getVictoire() {return this.gagne;}
    public int getIdUtilisateur() {return this.idut;}
    public int getIdScenario() {return this.idsc;}
}