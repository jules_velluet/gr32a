import java.util.ArrayList;
import java.util.List;

public class Administrateur implements Utilisateur {
    // Attributs
    private String pseudo;
    private String mail;
    private String motDePasse;
    private boolean connecte;
    private List<Joueur> listeJoueurs;
    private int idut;
    private byte[] avatar;
    private String RoleU;

    // Constructeur
    /**
     *
     * @param pseudo sous forme de String
     * @param mail sous forme de String
     * @param motDePasse sous forme de String
     * @param connecte sous forme de boolean
     */
    public Administrateur(String pseudo, String mail, String motDePasse, boolean connecte, byte[] avatar) {
        this.pseudo = pseudo;
        this.mail = mail;
        this.motDePasse = motDePasse;
        this.connecte = connecte;
        this.listeJoueurs = new ArrayList<>();
        this.avatar = avatar;
        this.RoleU = "Administrateur";
    }

    // Getters
    /**
     *
     * @return le pseudo de l'utilisateur
     */
    @Override
    public String getPseudo() {return this.pseudo;}
    
    /**
     *
     * @return l'avatar de l'utilisateur
     */
    @Override
    public byte[] getAvatar() {return this.avatar;}

    /**
     *
     * @return un boolean informant si l'utilisateur est connecté ou non
     */
    @Override
    public String getRoleU() {return this.RoleU;}

    /**
     *
     * @return le pseudo de l'utilisateur
     */
    @Override
    public int getId() {return this.idut;}

    /**
     *
     * @return l'adresse email de l'utilisateur
     */
    @Override
    public String getMail() {return this.mail;}

    /**
     *
     * @return le mot de passe de l'utilisateur
     */
    @Override
    public String getMotDePasse() {return this.motDePasse;}

    /**
     *
     * @return un boolean informant si l'utilisateur est connecté ou non
     */
    @Override
    public boolean estConnecte() {return this.connecte;}

    /**
     *
     * @return la liste complète des Joueurs
     */
    public List<Joueur> getJoueurs() {return this.listeJoueurs;}

    // Methods
    /**
     *
     * @param joueur sur lequel on souhaite modifier des informations
     * @param newPseudo (mettre à null si le pseudo ne souhaite pas être modifié)
     * @param newMail (mettre à null si l'adresse mail ne souhaite pas être modifié)
     * @param newMDP (mettre à null si le mot de passe ne souhaite pas être modifié)
     */
    public void gererUtilisateur(Joueur joueur, String newPseudo, String newMail, String newMDP) {
        for (Joueur j : this.listeJoueurs) {
            if (j.equals(joueur)) {
                if (newPseudo != null) {this.pseudo = newPseudo;}
                if (newMail != null) {this.mail = newMail;}
                if (newMDP != null) {this.motDePasse = newMDP;}
            }
        }
    }

    /**
     *
     * @return le nombre total de parties jouées parmi tous les joueurs
     */
    public int getNbParties() {
        int res = 0;
        for (Joueur joueur : this.listeJoueurs) {
            res += joueur.getNbPartiesJouees();
        }
        return res;
    }
}
