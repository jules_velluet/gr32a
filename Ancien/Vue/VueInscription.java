import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class VueInscription extends Application{
    private Stage st;
    private TextField tpseudo;
    private TextField tmail;
    private PasswordField tmdp;
    private PasswordField tconfirmMDP;
    private VueConnexion vueConnexion;

    public VueInscription(VueConnexion vueConnexion){
        this.vueConnexion = vueConnexion;
    }

    public static void main (String [] args){
        launch(args);
    }

    public void setScenePageInscription(Stage stage){
        this.st = stage;
        // Initialisation du texte
        Label lbienvenue = new Label("Bienvenue !");
        lbienvenue.setFont(Font.font("Arial", FontWeight.BOLD,50));

        // Initialisation des TextFields
        this.tpseudo = new TextField();
        this.tpseudo.setPromptText("Pseudo");
        this.tpseudo.setStyle("-fx-background-color: #c7c7c7ff; -fx-prompt-text-fill: #999999; -fx-background-radius: 10px");
        this.tpseudo.setPrefSize(350,40);


        this.tmail = new TextField();
        this.tmail.setPromptText("Adresse Mail");
        this.tmail.setStyle("-fx-background-color: #c7c7c7ff; -fx-prompt-text-fill: #999999; -fx-background-radius: 10px");
        this.tmail.setPrefSize(350,40);


        this.tmdp = new PasswordField();
        this.tmdp.setPromptText("Mot de Passe");
        this.tmdp.setStyle("-fx-background-color: #c7c7c7ff; -fx-prompt-text-fill: #999999; -fx-background-radius: 10px");
        this.tmdp.setPrefSize(350,40);


        this.tconfirmMDP = new PasswordField();
        this.tconfirmMDP.setPromptText("Confirmation du Mot de Passe");
        this.tconfirmMDP.setStyle("-fx-background-color: #c7c7c7ff; -fx-prompt-text-fill: #999999; -fx-background-radius: 10px");
        this.tconfirmMDP.setPrefSize(350,40);


        // Initialisation des boutons
        Button bValider = new Button("Valider l'inscription");
        bValider.setStyle("-fx-background-color: #6aa84fff; -fx-text-fill: white;-fx-background-radius: 10px");
        bValider.setPrefSize(200,20);
        bValider.setOnAction(new ControleurInscription(this.vueConnexion));

        // Initialisation vBox et Scene
        BorderPane borderPane = new BorderPane();
        VBox vBoxInscription = new VBox();
        vBoxInscription.getChildren().addAll(this.tpseudo,this.tmail,this.tmdp,this.tconfirmMDP,bValider);
        vBoxInscription.setSpacing(15);
        vBoxInscription.setAlignment(Pos.CENTER);
        borderPane.setTop(lbienvenue);
        borderPane.setCenter(vBoxInscription);
        borderPane.setAlignment(lbienvenue,Pos.TOP_CENTER);
        borderPane.setPadding(new Insets(40));
        Scene sceneInscription = new Scene(borderPane);
        this.st.setScene(sceneInscription);
    }


        public void start(Stage stage){
        this.st = stage;
        stage.setTitle("L'Échappée Belle - Administrateur");
        stage.setWidth(500);
        stage.setHeight(500);
        this.setScenePageInscription(this.st);
        stage.show();
    }
}
