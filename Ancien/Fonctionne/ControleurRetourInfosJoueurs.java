import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class ControleurRetourInfosJoueurs implements EventHandler<ActionEvent> {

    private VueScenarios vueScenarios;
    private VueInfosJoueur vueInfosJoueur;

    public ControleurRetourInfosJoueurs(VueInfosJoueur vueInfosJoueur){
        this.vueInfosJoueur = vueInfosJoueur;
        this.vueScenarios = this.vueInfosJoueur.getVueScenarios();
    }

    @Override
    public void handle(ActionEvent actionEvent){
        this.vueScenarios.SceneInit(this.vueInfosJoueur.getSt());
        this.vueScenarios.sceneInitSuite();
    }
}