import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
public class VueStatsJoueurs extends Application {

    private Stage st;
    private PasswordField mdp;
    private TextField tPseudo;
    private TextField tRole;
    private TextField tNbPartiesGagnees;
    private TextField tMail;
    private TextField tNbPartiesJouees;
    private TextField tNbPartiesPerdues;
    private TextField tPoucentageWin;

    public static void main(String[] args) {
        launch(args);
    }

    public void setSt(Stage st) {
        this.st = st;
    }

    public void setScenePageStatsJoueurs() {
        // Initialisation textes
        Label lModifInfos = new Label("Modifications des informations");
        lModifInfos.setPrefSize(50000, 20);
        lModifInfos.setAlignment(Pos.CENTER);
        lModifInfos.setStyle("-fx-text-fill: #000000; -fx-background-color: #ccccccff");

        // Initialisation des textfields
        this.tPseudo = new TextField();
        this.tPseudo.setPromptText("Pseudo");
        this.tPseudo.setPrefSize(175,50);
        this.tPseudo.setStyle("-fx-background-color: #d9d9d9ff");

        this.tMail = new TextField();
        this.tMail.setPromptText("Adresse Mail");
        this.tMail.setPrefSize(175,50);
        this.tMail.setStyle("-fx-background-color: #d9d9d9ff");


        this.mdp = new PasswordField();
        this.mdp.setPromptText("Mot de Passe");
        this.mdp.setPrefSize(175,50);
        this.mdp.setStyle("-fx-background-color: #d9d9d9ff");


        this.tRole = new TextField();
        this.tRole.setPromptText("Role");
        this.tRole.setPrefSize(175,50);
        this.tRole.setStyle("-fx-background-color: #d9d9d9ff");


        this.tNbPartiesJouees = new TextField();
        this.tNbPartiesJouees.setPromptText("Nombre de parties jouées");
        this.tNbPartiesJouees.setPrefSize(175,50);
        this.tNbPartiesJouees.setStyle("-fx-background-color: #d9d9d9ff");

        this.tNbPartiesGagnees = new TextField();
        this.tNbPartiesGagnees.setPromptText("Nombre de parties gagnées");
        this.tNbPartiesGagnees.setPrefSize(175,50);
        this.tNbPartiesGagnees.setStyle("-fx-background-color: #d9d9d9ff");

        this.tNbPartiesPerdues = new TextField();
        this.tNbPartiesPerdues.setPromptText("Nombre de parties perdues");
        this.tNbPartiesPerdues.setPrefSize(175,50);
        this.tNbPartiesPerdues.setStyle("-fx-background-color: #d9d9d9ff");

        this.tPoucentageWin = new TextField();
        this.tPoucentageWin.setPromptText("% de victoire");
        this.tPoucentageWin.setPrefSize(175,50);
        this.tPoucentageWin.setStyle("-fx-background-color: #d9d9d9ff");

        VBox vBox1 = new VBox();
        vBox1.getChildren().addAll(tPseudo,tMail,mdp,tRole);
        vBox1.setSpacing(20);
        vBox1.setAlignment(Pos.CENTER);

        VBox vBox2 = new VBox();
        vBox2.getChildren().addAll(tNbPartiesJouees,tNbPartiesGagnees,tNbPartiesPerdues,tPoucentageWin);
        vBox2.setSpacing(20);
        vBox2.setAlignment(Pos.CENTER);

        HBox hBox = new HBox();
        hBox.getChildren().addAll(vBox1,vBox2);
        hBox.setSpacing(50);
        hBox.setAlignment(Pos.CENTER);

        // Initialisation du bouton valider
        Button bValider = new Button("Valider");
        bValider.setStyle("-fx-background-color: #6aa84fff; -fx-text-fill: white; -fx-background-radius: 10px");
        bValider.setFont(Font.font("Arial",FontWeight.BOLD,20));
        bValider.setAlignment(Pos.CENTER);

        VBox vHaut = new VBox();
        vHaut.getChildren().add(lModifInfos);

        VBox vMilieu = new VBox();
        vMilieu.getChildren().addAll(hBox,bValider);
        vMilieu.setSpacing(30);
        vMilieu.setAlignment(Pos.CENTER);

        VBox vFinal = new VBox();
        vFinal.getChildren().addAll(vHaut,vMilieu);
        vFinal.setSpacing(30);

        Scene sceneGestionJoueursAdmin = new Scene(vFinal);
        this.st.setScene(sceneGestionJoueursAdmin);
    }

    public void start(Stage stage) {
        this.st = stage;
        stage.setTitle("L'Échappée Belle - Administrateur");
        stage.setWidth(500);
        stage.setHeight(500);
        this.setScenePageStatsJoueurs();
        stage.show();
    }
}

