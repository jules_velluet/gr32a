import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurInscription implements EventHandler<ActionEvent> {
    public VueConnexion vueConnexion;

    public ControleurInscription(VueConnexion vueConnexion){
        this.vueConnexion = vueConnexion;
    }
    @Override
    public void handle(ActionEvent actionEvent){
        this.vueConnexion.setScenePageConnexion();
    }

}
