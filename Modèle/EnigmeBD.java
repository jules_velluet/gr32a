import java.sql.*;
import java.util.List;
import java.util.ArrayList;
import javafx.scene.image.Image;
import java.io.ByteArrayInputStream;

public class EnigmeBD{

  private ConnexionMySQL laConnexion;

  public EnigmeBD(ConnexionMySQL laConexion){
    this.laConnexion = laConexion;
  }

  /**
    *
    * @param idEn(int, id de l'enigme)
    * @return le texte de l'egnime passer en parametre
  */
  public String ennoncerEnigme(int idEn) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select textEn from ENIGME where idEn="+idEn);
    rs.next();
    String res = rs.getString("textEn");
    rs.close();
    return res;
  }

  /**
    *
    * @param idEn(int, id de l'enigme)
    * @return le nom de l'énigme passer en parametre
  */
  public String nomEnigme(int idEn) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select nomEn from ENIGME where idEn="+idEn);
    rs.next();
    String res = rs.getString("nomEn");
    rs.close();
    return res;
  }

  /**
    *
    * @param idEn(int, id de l'enigme)
    * @return l'aide de l'énigme passer en parametre
  */
  public String aideEnigme(int idEn) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select aideEn from ENIGME where idEn="+idEn);
    rs.next();
    String res = rs.getString("aideEn");
    rs.close();
    return res;
  }

  /**
    *
    * @param idEn(int, id de l'enigme)
    * @return la réponse de l'énigme passer en parametre
  */
  public String reponseEn(int idEn) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select reponseEn from ENIGME where idEn="+idEn);
    rs.next();
    String res = rs.getString("reponseEn");
    rs.close();
    return res;
  }

  /**
    *
    * @param idEn(int, id de l'enigme)
    * @return true si l'énigme est un brouillon ou false sinon
  */
  public boolean brouillonEn(int idEn) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select brouillonEn from ENIGME where idEn="+idEn);
    rs.next();
    String michel = rs.getString("brouillonEn");
    rs.close();
    if (michel == "O"){
      return true;
    }
    else if (michel == "N"){
      return false;
    }
    else{
      throw new SQLException("valeur possible O ou N");
    }
  }

  public Image imgEn(int idEn) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select imgen from ENIGME where idEn="+idEn);
    rs.next();
    Image res = new Image(new ByteArrayInputStream(rs.getBytes("imgen")),200,200,true,true);
    rs.close();
    return res;
  }

  public int lig(int idca, int iden) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select lig from SITUER where idEn="+iden+" and idCa="+idca);
    rs.next();
    int res = rs.getInt("lig");
    rs.close();
    return res;
  }

  public int col(int idca, int iden) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select col from SITUER where idEn="+iden+" and idCa="+idca);
    rs.next();
    int res = rs.getInt("col");
    rs.close();
    return res;
  }

  public List<Integer> listeIdEnigme(int idca) throws SQLException{
    List<Integer> res = new ArrayList<>();
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select idEn from SITUER where idCa="+idca);
    while(rs.next()){
      res.add(rs.getInt("idEn"));
    }
    rs.close();
    return res;
  }


}
