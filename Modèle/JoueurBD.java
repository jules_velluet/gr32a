import java.sql.*;
import java.util.*;


public class JoueurBD{
    private ConnexionMySQL laConnexion;

    public JoueurBD(ConnexionMySQL laConnexion){
        this.laConnexion = laConnexion;
    }

    public List<Joueur> listeJoueurs() throws SQLException {
        List<Joueur> liste = new ArrayList<>();
        Statement st = laConnexion.createStatement();
        ResultSet rs = st.executeQuery("Select * from UTILISATEUR natural join ROLE where nomRole = 'Joueur'");
        Joueur Player = null;
        Boolean connecte;
        while (rs.next()){
            if (rs.getString("activeut") == "O"){connecte = true;} else {connecte = false;}
            Player = new Joueur(rs.getString("pseudout"),rs.getString("emailut"),rs.getString("mdput"),connecte,rs.getInt("idut"),
            rs.getBytes("avatarut"));
            liste.add(Player);
        }
        return liste;
    }
}