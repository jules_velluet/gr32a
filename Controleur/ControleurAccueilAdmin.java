import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class ControleurAccueilAdmin extends VBox{

    private VueGestionJoueursAdmin vueGestionJoueursAdmin;
    private VueGestionPartiesAdmin vueGestionPartiesAdmin;
    private VueAccueilAdmin vueAccueilAdmin;

    public ControleurAccueilAdmin(VueAccueilAdmin vueAccueilAdmin){
        this.vueGestionJoueursAdmin = new VueGestionJoueursAdmin();
        this.vueGestionPartiesAdmin = new VueGestionPartiesAdmin();
        this.vueAccueilAdmin = vueAccueilAdmin;
        Button bGérerJoueur = new Button("Gérer les joueurs");
        bGérerJoueur.setStyle("-fx-background-color: #999999ff; -fx-text-fill: white; -fx-background-radius: 20px");
        bGérerJoueur.setFont(Font.font("Arial", FontWeight.BOLD,20));
        bGérerJoueur.setPrefSize(350,50);
        bGérerJoueur.setOnAction(new ControleurGestionJoueurs());
        Button bGérerParties = new Button("Gérer les parties");
        bGérerParties.setStyle("-fx-background-color: #999999ff; -fx-text-fill: white;-fx-background-radius: 20px");
        bGérerParties.setFont(Font.font("Arial",FontWeight.BOLD,20));
        bGérerParties.setPrefSize(350,50);
        bGérerParties.setOnAction(new ControleurGestionParties());
        this.getChildren().addAll(bGérerJoueur,bGérerParties);
        this.setSpacing(15);
        this.setAlignment(Pos.CENTER);
    }

    public class ControleurGestionJoueurs implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent actionEvent){
            vueGestionJoueursAdmin.setSt(vueAccueilAdmin.getSt());
            vueGestionJoueursAdmin.setVueConnexion(vueAccueilAdmin.getVueConnexion());
            vueGestionJoueursAdmin.setScenePageGestionJoueursAdmin();
        }
    }

    public class ControleurGestionParties implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent actionEvent){
            vueGestionPartiesAdmin.setSt(vueAccueilAdmin.getSt());
            vueGestionPartiesAdmin.setVueConnexion(vueAccueilAdmin.getVueConnexion());
            vueGestionPartiesAdmin.setScenePageGestionPartiesAdmin();
        }
    }

}
