import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.PixelFormat;
import javafx.stage.FileChooser;
import java.io.File;

public class ControleurChoixImage implements EventHandler<ActionEvent> {
    private VueInscription vueInscription;

    public ControleurChoixImage(VueInscription vueInscription){
        this.vueInscription = vueInscription;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choix de l'image");
        fileChooser.setInitialDirectory(new File(System.getenv("HOME")));
        FileChooser.ExtensionFilter et = new FileChooser.ExtensionFilter("images","*.jpg");
        fileChooser.getExtensionFilters().add(et);
        File newImg = fileChooser.showOpenDialog(null);
        System.out.println(newImg.toString());
        if (newImg != null){
            this.vueInscription.setChemin(newImg.toString());
        }

    }
}
