import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class ControleurRetourAdminJoueursParties implements EventHandler<ActionEvent>{

    private VueAccueilAdmin vueAccueilAdmin;
    private VueGestionPartiesAdmin vueGestionPartiesAdmin;
    private VueGestionJoueursAdmin vueGestionJoueursAdmin;

    public ControleurRetourAdminJoueursParties(VueGestionPartiesAdmin vueGestionPartiesAdmin){
        this.vueAccueilAdmin = new VueAccueilAdmin();
        this.vueGestionPartiesAdmin = vueGestionPartiesAdmin;
    }

    public ControleurRetourAdminJoueursParties(VueGestionJoueursAdmin vueGestionJoueursAdmin){
        this.vueAccueilAdmin = new VueAccueilAdmin();
        this.vueGestionJoueursAdmin = vueGestionJoueursAdmin;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        if (this.vueGestionPartiesAdmin == null) {
            this.vueAccueilAdmin.setSt(this.vueGestionJoueursAdmin.getSt());
            this.vueAccueilAdmin.setScenePageAccueilAdmin(this.vueAccueilAdmin.getSt(),this.vueGestionJoueursAdmin.getVueConnexion());
        }
        else{
            this.vueAccueilAdmin.setSt(this.vueGestionPartiesAdmin.getSt());
            this.vueAccueilAdmin.setScenePageAccueilAdmin(this.vueAccueilAdmin.getSt(),this.vueGestionPartiesAdmin.getVueConnexion());
        }
    }

}
